const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const fizzBuzzController = require('./src/controller/fizzBuzzController');


const app = express();
// adding Helmet to enhance your API's security
app.use(helmet());

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// enabling CORS for all requests
app.use(cors());

// adding morgan to log HTTP requests
app.use(morgan('combined'));

app.listen(4000, () => {
 console.log("Server running on port 4000");
});

app.post("/fizz-buzz",fizzBuzzController.fizzBuzzCount)


