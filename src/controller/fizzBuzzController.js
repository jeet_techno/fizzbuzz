
const FillBuzz = require('../modules/FizzBuzzClass');
const fizzBuzzController = {
    
}

fizzBuzzController.fizzBuzzCount = async (req, res, next)=>{
    try{
        let count = parseInt(req.body.count) ;

        if(count==NaN){
            return res.status(500).send("Please enter a valid number");
        }

        if(count<1){
            return res.status(500).send("Please enter a valid positive count");
        }

        let fizzBuzzInstance = new FillBuzz()
        let response = await fizzBuzzInstance.getFizzBuzzCount(count)
        //console.log(response)
        return res.status(200).send(response);
    }catch(e){
        console.log(e)
        return res.status(500).send("Something went wrong");
    }
    
}

module.exports = fizzBuzzController;