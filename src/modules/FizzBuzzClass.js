const { ResumeToken } = require('mongodb');
const fillBuzzConfig = require('../../src/config/fizzBuzzConfig')
class FillBuzz{
    __constructor(){

    }

    getFizzBuzzCount=(count)=>{
        let result = [];

        for(let i=1;i<=count;i++){
            //
            if(this.fizzBuzzAction(i) == true){
                result.push('FizzBuzz');
                continue;
            }

            if(this.buzzAction(i) == true){
                result.push('Buzz');
                continue;
            }

            if(this.fizzAction(i)==true){
                result.push('Fizz');
                continue;
            }

            console.log(i)
            result.push(i);

        }

        return result;
    }

    fizzAction = (caountVal)=>{
        return (caountVal % fillBuzzConfig.fizz)== 0? true: false;
    }

    buzzAction=(caountVal)=>{
        return (caountVal%fillBuzzConfig.buzz) == 0 ? true: false;
    }

    fizzBuzzAction =(caountVal)=>{
        return (caountVal%(fillBuzzConfig.fizz * fillBuzzConfig.buzz)) == 0 ? true: false;
    }
}

module.exports = FillBuzz;